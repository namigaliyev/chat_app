namespace chat_app.Coore.HTTP.Request
{
    public class LoginRequest
    {
        public string  Username { get; set; }

        public string  Password { get; set; }
    }
}