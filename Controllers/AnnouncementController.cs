using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using chat_app.Data;
using chat_app.Hubs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace chat_app.Controllers
{
    [Route("api/[controller]")]
    
    [Authorize]
    public class AnnouncementController : Controller
    {
        private ChatDbContext context;

        public AnnouncementController(ChatDbContext context)
        {
            this.context = context;
        }

        [HttpGet]
        [Route("index")]
        public IEnumerable<string> Index()
        {
            
            return context.Users.Select(x => x.UserName).ToArray();
        }
    }
}