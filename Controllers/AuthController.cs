using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using chat_app.Coore.HTTP.Request;
using chat_app.Data;
using chat_app.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace chat_app.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private UserManager<User> userManager;
        private ChatDbContext dbContext;

        public AuthController(UserManager<User> userManager, ChatDbContext dbContext)
        {
            this.userManager = userManager;
            this.dbContext = dbContext;
        }

        [HttpGet]
        [Route("index")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest model)
        {
            User user = await userManager.FindByNameAsync(model.Username);
            
            if(user != null && await userManager.CheckPasswordAsync(user, model.Password))
            {
                Claim[] claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };

                SymmetricSecurityKey signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("MySuperSecureKey"));
                
                JwtSecurityToken token = new JwtSecurityToken(
                    issuer: "namig",
                    audience: "namig",
                    expires: DateTime.UtcNow.AddHours(1),
                    claims: claims,
                    signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
                );
                return RedirectToAction("index","Announcement");
            }
            return View();
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegistrationRequest model)
        {
            User user = await userManager.FindByNameAsync(model.Username);
            
            if(user == null)
            {
                User users = new User()
                {
                    Email = model.Email,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = model.Username
                };
                await userManager.CreateAsync(users, model.Password);
            }
            return Ok();
        }
    }
}