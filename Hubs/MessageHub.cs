using System;
using System.Threading.Tasks;
using chat_app.Data;
using chat_app.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace chat_app.Hubs
{
    [Authorize]
    public class MessageHub: Hub
    {
        private ChatDbContext dbContext;

        public MessageHub(ChatDbContext dbContext)
        {
           this.dbContext = dbContext;
        }
        public async Task SendMessageToAll(string user,string message)
        {
            // Message messageObj = new Message()
            // {
            //     Username = user,
            //     MessageContent = message,
            //     MessageDate = DateTime.Now
            // };
            // dbContext.Messages.Add(messageObj);
            // await dbContext.SaveChangesAsync();
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }

        public Task SendMessageToCaller(string message)
        {
            return Clients.Caller.SendAsync("ReceiveMessage", message);
        }

        public Task SendMessageToUser(string connectionId, string message)
        {
            return Clients.Client(connectionId).SendAsync("ReceiveMessage", message);
        }

        public Task JoinGroup(string group)
        {
            return Groups.AddToGroupAsync(Context.ConnectionId, group);
        }

        public Task SendMessageToGroup(string group, string message)
        {
            return Clients.Group(group).SendAsync("ReceiveMessage", message);
        }
        public override async Task OnConnectedAsync()
        {
            UserHandler.ConnectedIds.Add(Context.ConnectionId);
            await Clients.All.SendAsync("UserConnected", Context.ConnectionId);
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception ex)
        {
            UserHandler.ConnectedIds.Remove(Context.ConnectionId);
            await Clients.All.SendAsync("UserDisconnected", Context.ConnectionId);
            await base.OnDisconnectedAsync(ex);
        }
    }
}