using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace chat_app.Models
{
    public class User : IdentityUser
    {
        public ICollection<Connection> Connections { get; set; }
    }
}