using System;
using System.Linq;
using chat_app.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace chat_app.Data
{
    public class SeedDatabase
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            ChatDbContext context  = serviceProvider.GetRequiredService<ChatDbContext>();
            UserManager<User> userManager  = serviceProvider.GetRequiredService<UserManager<User>>();
            context.Database.EnsureCreated();
            
            if(!context.Users.Any())
            {
                User user = new User()
                {
                    Email = "asd@.com",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = "Ram"
                };
                userManager.CreateAsync(user, "Password1232");
            }
        }
    }
}